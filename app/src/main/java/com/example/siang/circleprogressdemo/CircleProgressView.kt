package com.example.siang.circleprogressdemo

import android.animation.Animator
import android.animation.ObjectAnimator
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.text.TextPaint
import android.util.AttributeSet
import android.util.Log
import android.util.TypedValue
import android.view.MotionEvent
import android.view.View
import java.util.concurrent.TimeUnit

/**
 * Author: siang
 * Created: 2018/5/15
 */
class CircleProgressView : View, View.OnTouchListener {

    private lateinit var mProgressbarPaint: Paint
    private lateinit var mCircleButtonPaint: Paint
    private lateinit var mCircleButtonTextPaint: TextPaint
    private lateinit var mCircleButtonRect: RectF
    private lateinit var mProgressbarRect: RectF
    private lateinit var mCircleButtonText: String
    private var mProgressbarThickness = 2f
    private var mCircleButtonSpacing = 0
    private var mClockwiseObjectAnimator: ObjectAnimator? = null // 順時針方向動畫
    private var mCounterclockwiseObjectAnimator: ObjectAnimator? = null // 逆時針方向動畫
    private var mAnimationDuration = 1.5f // 動畫時間
    private var mIsClockWise = false

    private var progress = 0f // 0~100
        set(value) {
            Log.i("siang", "progress = $value")

            // 進度條完成100%
            // 因逆時針動畫會從100開始，所以要先判斷是否為順時針動畫
            field = if (value.toInt() == 100 && mIsClockWise) {
                progressFinishListener?.invoke()
                mCircleButtonRect.set(0f, 0f, height.toFloat(), height.toFloat())
                0f
            } else {
                value
            }
            invalidate()
        }
    var progressFinishListener: (() -> Unit)? = null

    private val mClockwiseAnimatorListener = object : Animator.AnimatorListener {
        override fun onAnimationRepeat(animation: Animator?) {
        }

        override fun onAnimationEnd(animation: Animator?) {
            mIsClockWise = false
        }

        override fun onAnimationCancel(animation: Animator?) {
        }

        override fun onAnimationStart(animation: Animator?) {
            mIsClockWise = true

            // 將圓形按鈕縮小
            mCircleButtonRect.set(0 + mProgressbarThickness + mCircleButtonSpacing,
                    0 + mProgressbarThickness + mCircleButtonSpacing,
                    height - mProgressbarThickness - mCircleButtonSpacing,
                    height - mProgressbarThickness - mCircleButtonSpacing)
        }
    }

    private val mCounterclockwiseAnimatorListener = object : Animator.AnimatorListener {
        override fun onAnimationRepeat(animation: Animator?) {
        }

        override fun onAnimationEnd(animation: Animator?) {
            // 動畫結束後，需將圓形按鈕恢復原大小
            mCircleButtonRect.set(0f, 0f, height.toFloat(), height.toFloat())
        }

        override fun onAnimationCancel(animation: Animator?) {
        }

        override fun onAnimationStart(animation: Animator?) {
        }
    }

    constructor(context: Context, attrs: AttributeSet): super(context, attrs) {
        init(context, attrs, R.attr.CircleProgressView)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int): super(context, attrs, defStyleAttr) {
        init(context, attrs, defStyleAttr)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int):
            super(context, attrs, defStyleAttr, defStyleRes) {
        init(context, attrs, defStyleAttr)
    }

    private fun init(context: Context, attrs: AttributeSet, defStyleAttr: Int) {
        setOnTouchListener(this)

        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.CircleProgressView, defStyleAttr, R.style.AppTheme_CircleProgressView)
        val circleButtonColor = typedArray.getColor(R.styleable.CircleProgressView_circleButtonColor, Color.RED)
        val progressbarColor = typedArray.getColor(R.styleable.CircleProgressView_progressbarColor, Color.RED)
        val circleButtonTextSize = typedArray.getDimensionPixelSize(R.styleable.CircleProgressView_circleButtonTextSize, spToPixels(20f))
        val circleButtonTextColor = typedArray.getColor(R.styleable.CircleProgressView_circleButtonTextColor, Color.BLUE)
        mCircleButtonText = typedArray.getString(R.styleable.CircleProgressView_circleButtonText)
        mAnimationDuration = typedArray.getFloat(R.styleable.CircleProgressView_progressbarDuration, 1.5f)
        mProgressbarThickness = typedArray.getDimension(R.styleable.CircleProgressView_progressbarThickness, mProgressbarThickness)
        mCircleButtonSpacing = typedArray.getDimensionPixelSize(R.styleable.CircleProgressView_circleButtonSpacing, dpToPixels(10f))
        typedArray.recycle()

        mCircleButtonRect = RectF()
        mProgressbarRect = RectF()
        mCircleButtonPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
            color = circleButtonColor
        }
        mProgressbarPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
            style = Paint.Style.STROKE
            color = progressbarColor
            strokeWidth = mProgressbarThickness
        }
        mCircleButtonTextPaint = TextPaint().apply {
            textAlign = Paint.Align.CENTER
            textSize = circleButtonTextSize.toFloat()
            color = circleButtonTextColor
        }
    }

    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        when (event?.action) {
            MotionEvent.ACTION_DOWN -> {
                // 順時針動畫: 開始
                // 逆時針動畫: 取消
                if (mCounterclockwiseObjectAnimator?.isRunning == true) {
                    mCounterclockwiseObjectAnimator?.cancel()
                }
                mClockwiseObjectAnimator = ObjectAnimator.ofFloat(this, "progress", progress, 100f).apply {
                    // 每一刻度所需秒數 * 未跑完進度
                    duration = (TimeUnit.SECONDS.toMillis(mAnimationDuration.toLong()) / 100f * (100 - progress)).toLong()
                    addListener(mClockwiseAnimatorListener)
                }
                mClockwiseObjectAnimator?.start()
            }
            MotionEvent.ACTION_UP -> {
                // 順時針動畫: 取消
                // 逆時針動畫: 開始
                if (mClockwiseObjectAnimator?.isRunning == true) {
                    mClockwiseObjectAnimator?.cancel()
                }
                mCounterclockwiseObjectAnimator = ObjectAnimator.ofFloat(this, "progress", progress, 0f).apply {
                    // 每一刻度所需秒數 * 已跑完進度
                    duration = (TimeUnit.SECONDS.toMillis(mAnimationDuration.toLong()) / 100f * progress).toLong()
                    addListener(mCounterclockwiseAnimatorListener)
                }
                mCounterclockwiseObjectAnimator?.start()
            }
        }
        return true
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val width = getDefaultSize(suggestedMinimumWidth, widthMeasureSpec)
        val height = getDefaultSize(suggestedMinimumHeight, heightMeasureSpec)
        val min = Math.min(width, height)
        setMeasuredDimension(min, min)

        mCircleButtonRect.set(0f, 0f, min.toFloat(), min.toFloat())
        mProgressbarRect.set(0 + mProgressbarThickness / 2, 0 + mProgressbarThickness / 2,
                min - mProgressbarThickness / 2, min - mProgressbarThickness / 2)
    }

    override fun onDraw(canvas: Canvas?) {
        canvas ?: return

        // 圓形按鈕
        canvas.drawCircle((mCircleButtonRect.left + mCircleButtonRect.right) / 2,
                (mCircleButtonRect.top + mCircleButtonRect.bottom) / 2,
                mCircleButtonRect.width() / 2, mCircleButtonPaint)

        // 進度條
        val angle = 360 * (progress / 100)
        canvas.drawArc(mProgressbarRect, -90f, angle, false, mProgressbarPaint)

        // 圓形按鈕文字
        // 矩形中心與基線的距離
        val distance: Float = mCircleButtonTextPaint.fontMetrics.run {
            (bottom - top) / 2 - bottom
        }
        // y = 矩形中心y值 + 矩形中心與基線的距離
        canvas.drawText(mCircleButtonText, width / 2f, height / 2f + distance, mCircleButtonTextPaint)
    }

    private fun spToPixels(sp: Float): Int {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, resources.displayMetrics).toInt()
    }

    private fun dpToPixels(dp: Float): Int {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, resources.displayMetrics).toInt()
    }
}